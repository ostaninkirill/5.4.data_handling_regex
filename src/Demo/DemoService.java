package src.Demo;

import src.Service.StringService;
import src.Service.IStringService;

public class DemoService implements IDemoService {
    @Override
    public void execute() {
        IStringService ss = new StringService();

        // Задача 4.1.
        System.out.println("Задача 4.1. Слова, которые содержатся в строке: ");
        System.out.println(ss.getWords("Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?"));
        System.out.println();

        // Задача 4.2.
        System.out.print("Задача 4.2. Самое короткое слово из строки задания 4.1: ");
        System.out.println(ss.getLongestWord("Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?"));
        System.out.print("Самое длинное: ");
        System.out.println(ss.getAShortWord("Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?"));
        System.out.println();

        // Задача 4.3.
        System.out.println("Задача 4.3.");
        System.out.println("Удаление в строке префиксов локальных номеров, соответствующих Ижевску:  ");
        System.out.println(ss.phoneNumber("Дана строка, содержащая в себе, помимо прочего, номера телефонов. Необходимо удалить из этой строки префиксы локальных номеров, \n" +
                "соответствующих Ижевску. Например, из \"+7 (3412) 517-647\" получить \"517-647\"; \"8 (3412) 4997-12\" > \"4997-12\"; \"+7 3412 90-41-90\" > \"90-41-90\""));
        System.out.println();

        // Задача 4.4.
        System.out.println("Задача 4.4.");
        String text = "Уважаемый, $userName, извещаем вас о том, что на вашем счете $номерСчета скопилась сумма, превышающая стоимость \n" +
                "$числоМесяцев месяцев пользования нашими услугами. Деньги продолжают поступать. Вероятно, вы неправильно настроили \n" +
                "автоплатеж. С уважением, $пользовательФИО $должностьПользователя";
        String key = "userName, номерСчета, числоМесяцев, пользовательФИО, должностьПользователя";
        String value = "Кирилл, 1337112495, 12, Иоганн Себастьян Бах, менеджер";
        System.out.println(ss.patternString(text,key,value));

    }
}
