package src.Service;

public interface IStringService {
    String getWords (String text);
    String getLongestWord(String text);
    String getAShortWord(String text);
    String phoneNumber(String text);
    String patternString(String text, String templateKey, String templateValue);
}
