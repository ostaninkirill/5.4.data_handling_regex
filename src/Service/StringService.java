package src.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringService implements IStringService {

    @Override
    public String getWords(String text) {
        Pattern p = Pattern.compile("[а-яА-Яa-zA-Z']+");
        StringBuilder result = new StringBuilder();
        Matcher m = p.matcher(text);

        while ( m.find() ) {
            result.append(text.substring(m.start(), m.end()).toLowerCase()).append("\n");
        }
        return result.toString();
    }

    @Override
    public String getLongestWord(String text) {
        String[] words = getWords(text).split("\n");
        String longWord = "";
        for(String word : words) {
            if (word.length() > longWord.length()) longWord = word;
        }
        return longWord;
    }

    @Override
    public String getAShortWord(String text) {
        String[] words = getWords(text).split("\n");
        String shortWord = words[0];
        for(String word : words) {
            if (word.length() < shortWord.length()) shortWord = word;
        }
        return shortWord;
    }

    @Override
    public String phoneNumber(String text) {
        return text.replaceAll("((\\+7||8)?\\s*\\(?3412\\)? )", "");


    }

    @Override
    public String patternString(String text, String templateKey, String templateValue) {
        String[] keys = templateKey.split(", ");
        String[] values = templateValue.split(", ");
        for (int i = 0; i < keys.length; i++) {
            text = text.replaceAll("\\$"+keys[i], values[i]);
        }
        return text;
    }


}
